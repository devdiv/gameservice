# Mosar

## A flashcard application

Mosar is an application to help you train for your exams by means of flashcard practice.

## Copyright notice
Copyright 2021, Inquisitive BV

## GPL V3 License
This code can be used and distributed under GPL V3 license. For more information about GPL V3: http://www.gnu.org/licenses/gpl-3.0.html
## Set it up locally

To set it up locally all you need to to is run the following commands:

`git clone`

`git checkout develop`

`docker-compose up`

Then you will see it run on [localhost](http://localhost:4200)

## Please note
This is a work in progress. The first official release is coming up soon. We wil also work some some new features and more exams to study. For now, just enjoy and let us know what you think!

Trigger +1