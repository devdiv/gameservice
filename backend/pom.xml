<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>mosar</groupId>
    <artifactId>gameservice</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>jar</packaging>
    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <start-class>nl.inquisitive.mosar.game.GameApplication</start-class>
        <failOnMissingWebXml>false</failOnMissingWebXml>
        <version.maven-surefire-plugin>3.0.0-M6</version.maven-surefire-plugin>
        <version.spring-boot-starter-web>2.6.5</version.spring-boot-starter-web>
        <version.spring-boot-starter-test>2.6.7</version.spring-boot-starter-test>
        <version.modelmapper>3.1.0</version.modelmapper>
        <version.lombok>1.18.22</version.lombok>
        <version.openapi-generator>5.4.0</version.openapi-generator>
        <version.jackson-databind-nullable>0.2.2</version.jackson-databind-nullable>
        <version.jackson-databind>2.13.2.2</version.jackson-databind>
        <version.jackson-core>2.13.2</version.jackson-core>
        <version.maven-compiler-plugin>3.10.1</version.maven-compiler-plugin>
        <!-- Change the name of the target branch for the open api schema here-->
        <git.branchname>main</git.branchname>
        <api-version>v2022-04-05</api-version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>${version.spring-boot-starter-web}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>${version.spring-boot-starter-test}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.modelmapper</groupId>
            <artifactId>modelmapper</artifactId>
            <version>${version.modelmapper}</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${version.lombok}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>openapi-generator</artifactId>
            <version>${version.openapi-generator}</version>
        </dependency>
        <dependency>
            <groupId>org.openapitools</groupId>
            <artifactId>jackson-databind-nullable</artifactId>
            <version>${version.jackson-databind-nullable}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${version.jackson-databind}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
            <version>${version.jackson-core}</version>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
            <version>${version.jackson-core}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>${version.maven-compiler-plugin}</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>RELEASE</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.openapitools</groupId>
                <artifactId>openapi-generator-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                        <configuration>
                            <inputSpec>
                                https://gitlab.com/api/v4/projects/35073288/repository/files/openapi%2FgameService%2F${api-version}%2Fschema.yaml/raw?ref=${git.branchname}
                            </inputSpec>
                            <generatorName>spring</generatorName>
                            <apiPackage>nl.inquisitive.mosar.game.controller.api</apiPackage>
                            <modelPackage>nl.inquisitive.mosar.game.controller.model</modelPackage>
                            <supportingFilesToGenerate>
                                ApiUtil.java
                            </supportingFilesToGenerate>
                            <configOptions>
                                <delegatePattern>true</delegatePattern>
                            </configOptions>
                            <output>target/classes</output>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${version.maven-surefire-plugin}</version>
            </plugin>
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <configuration>
                    <delimiters>
                        <delimiter>@</delimiter>
                    </delimiters>
                    <useDefaultDelimiters>false</useDefaultDelimiters>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>11</source>
                    <target>11</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-dependency-plugin</artifactId>
                <configuration>
                    <usedDependencies>
                        <dependency>org.springframework.boot:spring-boot-starter-web</dependency>
                        <dependency>org.openapitools:openapi-generator</dependency>
                        <dependency>org.projectlombok:lombok</dependency>
                        <dependency>org.mockito:mockito-junit-jupiter</dependency>
                    </usedDependencies>
                </configuration>
            </plugin>
        </plugins>
    </build>
    <profiles>
        <profile>
            <id>owasp-dependency-check</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.owasp</groupId>
                        <artifactId>dependency-check-maven</artifactId>
                        <configuration>
                            <failBuildOnCVSS>7</failBuildOnCVSS>
                            <suppressionFiles>
                                <suppressionFile>dc-suppress.xml</suppressionFile>
                            </suppressionFiles>
                        </configuration>
                        <executions>
                            <execution>
                                <goals>
                                    <goal>check</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
