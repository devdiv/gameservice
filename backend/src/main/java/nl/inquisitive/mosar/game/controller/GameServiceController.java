package nl.inquisitive.mosar.game.controller;

import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.game.controller.api.SequenceApiDelegate;
import nl.inquisitive.mosar.game.controller.model.GameSequence;
import nl.inquisitive.mosar.game.service.GameServiceLayer;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
@Slf4j
public class GameServiceController implements SequenceApiDelegate {

  @Autowired
  GameServiceLayer gameServiceLayer;

  @Override
  public ResponseEntity<GameSequence> getGameSequence(String courseId) {
    log.debug("Starting getGameSequence with courseId" + courseId);
    GameSequence gameSequence = new GameSequence();
    gameSequence.setCourseid(courseId);
    gameSequence.setSequence(gameServiceLayer.getTheLengthForAGivenCourse(courseId));
    log.debug("Request has been build for courseId" + courseId);
    return ResponseEntity.ok().body(gameSequence);
  }
}
