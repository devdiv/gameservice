package nl.inquisitive.mosar.game.exceptions;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@Slf4j
public abstract class AbstractExceptionHandler extends ResponseEntityExceptionHandler {

    @Setter
    public HttpStatus httpStatus;

    @Setter
    public boolean showErrorDetails;

    public ResponseEntity HandleException(Exception exception, WebRequest webRequest) {
        ErrorDetails errorDetails = new ErrorDetails(new Date(), exception.getMessage(),
                webRequest.getDescription(true));
        ErrorDetails errorDetailsLowInformation = new ErrorDetails(new Date(), httpStatus.toString(),
                webRequest.getDescription(false));
        log.error(errorDetails.toString());
        return (showErrorDetails) ? new ResponseEntity<>(errorDetails, httpStatus) : new ResponseEntity<>(errorDetailsLowInformation,httpStatus);
    }
}
