package nl.inquisitive.mosar.game.exceptions;

import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@ToString
public class ErrorDetails {
  @Getter
  private final Date timestamp;
  @Getter
  private final String message;
  @Getter
  private final String details;

  public ErrorDetails(Date timestamp, String message, String details) {
    this.timestamp = timestamp;
    this.message = message;
    this.details = details;
  }
}
