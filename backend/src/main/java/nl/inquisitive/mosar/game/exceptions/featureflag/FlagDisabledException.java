package nl.inquisitive.mosar.game.exceptions.featureflag;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class FlagDisabledException
        extends RuntimeException {

    public FlagDisabledException(String flag) {
        super("Feature Flag " + flag + " is disabled");
    }
}
