package nl.inquisitive.mosar.game.exceptions.featureflag;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class FlagNotFoundException
        extends RuntimeException {

    public FlagNotFoundException(String flag) {
        super("Feature flag " + flag + " was not found");
    }
}
