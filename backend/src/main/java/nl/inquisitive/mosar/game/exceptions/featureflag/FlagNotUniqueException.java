package nl.inquisitive.mosar.game.exceptions.featureflag;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.CONFLICT)
public class FlagNotUniqueException
        extends RuntimeException {

    public FlagNotUniqueException(String flag) {
        super("Feature flag " + flag + " has no unique description");
    }
}

