package nl.inquisitive.mosar.game.exceptions.responses;

import nl.inquisitive.mosar.game.exceptions.AbstractExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.WebRequest;


@ControllerAdvice
public class BadRequestException extends AbstractExceptionHandler {

    @Override
    @ExceptionHandler(HttpClientErrorException.BadRequest.class)
    public ResponseEntity HandleException(Exception exception, WebRequest webRequest) {
        setHttpStatus(HttpStatus.BAD_REQUEST);
        return super.HandleException(exception, webRequest);
    }
}
