package nl.inquisitive.mosar.game.exceptions.responses;

import nl.inquisitive.mosar.game.exceptions.AbstractExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class CaughtException extends AbstractExceptionHandler {

    @Override
    @ExceptionHandler({Exception.class})
    public ResponseEntity HandleException(Exception exception, WebRequest webRequest) {
        setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return super.HandleException(exception, webRequest);
    }
}
