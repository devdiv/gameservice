package nl.inquisitive.mosar.game.exceptions.responses;

import nl.inquisitive.mosar.game.exceptions.AbstractExceptionHandler;
import nl.inquisitive.mosar.game.exceptions.featureflag.FlagDisabledException;
import nl.inquisitive.mosar.game.exceptions.featureflag.FlagNotFoundException;
import nl.inquisitive.mosar.game.exceptions.featureflag.FlagNotUniqueException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class FeatureFlagException extends AbstractExceptionHandler {

    @Override
    @ExceptionHandler({FlagDisabledException.class, FlagNotFoundException.class, FlagNotUniqueException.class})
    public ResponseEntity HandleException(Exception exception, WebRequest webRequest) {
        setShowErrorDetails(true);
        setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
        return super.HandleException(exception, webRequest);
    }
}
