package nl.inquisitive.mosar.game.maxsequence;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;

@Slf4j
public class CurrentMaxSequence {

    //Tread safe double locked singleton
    private static CurrentMaxSequence uniqueCurrentMaxSequence;
    @Setter
    @Getter
    Collection<Object[]> allMaxValuesOfAllSets = new ArrayList<>();
    @Getter
    @Setter
    private Integer currentFlashcardMaxSequence;

    public static CurrentMaxSequence getInstance() {
        if (uniqueCurrentMaxSequence == null) {
            synchronized (CurrentMaxSequence.class) {
                if (uniqueCurrentMaxSequence == null) {
                    uniqueCurrentMaxSequence = new CurrentMaxSequence();
                }
            }
        }
        return uniqueCurrentMaxSequence;
    }

    //add a collection to the singleton
    public void addCollectionToSingleton(String courseId, String cleanedBody, Instant now) {
        log.debug("Entering addCollectionToSingleton with variables " + courseId + " " + cleanedBody + " " + now);
        // Current max sequence singleton
        Collection<Object[]> allMaxValuesOfAllSets = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets();
        // object from parsed http request string
        Object[] toBeAddedList = {courseId, cleanedBody, now};
        // add parsed object to max sequence singleton object
        allMaxValuesOfAllSets.add(toBeAddedList);
        // update the singleton
        CurrentMaxSequence.getInstance().setAllMaxValuesOfAllSets(allMaxValuesOfAllSets);
        log.debug("Variables have been added");
    }

    //remove a set
    public void removeASetFromSingleton(String courseId, String cleanedBody, Instant now) {
        log.debug("Entering removeASetFromSingleton with variables " + courseId + " " + cleanedBody + " " + now);
        Collection<Object[]> currentMax = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets();
        Object[] toBeRemovedList = {courseId, cleanedBody, now};
        currentMax.remove(toBeRemovedList);
        CurrentMaxSequence.getInstance().setAllMaxValuesOfAllSets(currentMax);
        log.debug("Set in Singleton has been removed");
    }
}
