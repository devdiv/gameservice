package nl.inquisitive.mosar.game.models;

import lombok.Data;

@Data
public class GetMaxFromFlashcardResponse {
    //Sad DTO is sad :(
    Integer maxFlashcards;
}
