package nl.inquisitive.mosar.game.sequencegenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
public class SequenceGenerator {

    @Getter
    @Setter
    List<Integer> gameSequence;

    public SequenceGenerator(int sequenceMax) {
        log.debug("Starting Sequence Generator with int sequenceMax " + sequenceMax);
        //1 to max make a list
        List<Integer> generatedSequence = new ArrayList<>();
        for (int i = 0; i < sequenceMax; i++) {
            generatedSequence.add(i);
        }
        // Shuffel
        Collections.shuffle(generatedSequence);

        //set the sequence
        log.debug("Setting generated sequence");
        setGameSequence(generatedSequence);
    }
}
