package nl.inquisitive.mosar.game.service;

import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.game.models.GetMaxFromFlashcardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Component
@Slf4j
public class FlashcardRequestService {

    @Value("${url.flashcard}")
    String urlFlashcard;

    @Autowired
    RestTemplate restTemplate;

    public String requestTheMaxForACourseFromFlashcardService(String courseId) {
        log.debug("requesting service on " + this.urlFlashcard + "/api/flashcards/flashcard/maxavailable/" + courseId);
        ResponseEntity<GetMaxFromFlashcardResponse> getMaxFromFlashcardResponseResponseEntity = restTemplate.getForEntity(this.urlFlashcard + "/api/flashcards/flashcard/maxavailable/" + courseId, GetMaxFromFlashcardResponse.class);
        return String.valueOf((Arrays.asList(getMaxFromFlashcardResponseResponseEntity.getBody())).get(0).getMaxFlashcards());
    }


}
