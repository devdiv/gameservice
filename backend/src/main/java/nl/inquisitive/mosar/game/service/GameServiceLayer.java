package nl.inquisitive.mosar.game.service;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.inquisitive.mosar.game.maxsequence.CurrentMaxSequence;
import nl.inquisitive.mosar.game.sequencegenerator.SequenceGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.Instant;
import java.util.List;

@Service
@Slf4j
public class GameServiceLayer {
    @Autowired
    FlashcardRequestService flashcardRequestService;
    @Getter
    @Setter
    private String currentWorkingMax;
    @Getter
    @Setter
    private Instant currentWorkingInstantTime;

    public List<Integer> getTheLengthForAGivenCourse(String courseId) {
        return generateNewSequenceForACourse(Integer.parseInt(maxOfACourse(courseId)));
    }

    //Return the sequence for a given length
    private List<Integer> generateNewSequenceForACourse(int lengthOfTheSet) {
        return new SequenceGenerator(lengthOfTheSet).getGameSequence();
    }

    private String requestTheMaxOfACourse(String courseId) {
        return flashcardRequestService.requestTheMaxForACourseFromFlashcardService(courseId);
    }

    //What is the current game sequence for a given course?
    private String maxOfACourse(String courseId) {
        log.debug("Starting MaxOfACourse with courseId " + courseId);
        setCurrentWorkingInstantTime(Instant.now());

        //List is empty no request has been done
        //if set doesnt exists
        if (CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets().isEmpty() || !setExistsInSingleton(courseId)) {
            //request and add to singleton variable
            log.debug("Singleton is empty or doesnt exist in singleton");
            setCurrentWorkingMax(requestTheMaxOfACourse(courseId));
            CurrentMaxSequence.getInstance().addCollectionToSingleton(courseId, getCurrentWorkingMax(), getCurrentWorkingInstantTime());
        }
        //if it is found but old do a new request
        if (isDataOlderThan15Minutes(getCurrentWorkingInstantTime())) {
            log.debug("Data is older then 15 minutes");
            //request the service and set the max
            setCurrentWorkingMax(requestTheMaxOfACourse(courseId));
            log.debug("Request has been processed");
            //Remove data
            CurrentMaxSequence.getInstance().removeASetFromSingleton(courseId, getCurrentWorkingMax(), getCurrentWorkingInstantTime());
            log.debug("Data removal from singleton has been processed");
            //add data
            CurrentMaxSequence.getInstance().addCollectionToSingleton(courseId, getCurrentWorkingMax(), getCurrentWorkingInstantTime());
            log.debug("Request added to singleton");
        }
        //data has been processed for return
        log.debug("Max sequence request has been processed");
        return getCurrentWorkingMax();
    }

    private boolean setExistsInSingleton(String courseId) {
        log.debug("Set setExistsInSingleton starting with courseId " + courseId);
        //get the singleton, move it to an array
        Object[] arrayFromCurrentMaxSequenceInstance = CurrentMaxSequence.getInstance().getAllMaxValuesOfAllSets()
                .toArray();
        //Loop over all the maxes in the singleton
        for (Object objectArrayFromASetId : arrayFromCurrentMaxSequenceInstance) {
            //If the course id exists in set
            if (((Object[]) objectArrayFromASetId)[0].equals(courseId)) {
                //It exists in set, so return and exit function
                //Set all the working instant variables
                setCurrentWorkingMax(((String) ((Object[]) objectArrayFromASetId)[1]));
                setCurrentWorkingInstantTime((Instant) ((Object[]) objectArrayFromASetId)[2]);
                log.debug("Course has been found in singleton");
                return true;
            }
        }
        //Set is not found
        log.debug("Course has NOT been found in singleton");
        return false;
    }

    //What if the data is older than 15 minutes?
    private boolean isDataOlderThan15Minutes(Instant instantTobeCompared) {
        return Duration.between(instantTobeCompared, Instant.now()).getSeconds() > 60 * 15;
    }
}
