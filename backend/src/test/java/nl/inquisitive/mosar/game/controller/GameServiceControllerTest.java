package nl.inquisitive.mosar.game.controller;

import nl.inquisitive.mosar.game.controller.model.GameSequence;
import nl.inquisitive.mosar.game.service.GameServiceLayer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest(classes = GameServiceController.class)
@ExtendWith(SpringExtension.class)
class GameServiceControllerTest {

    @MockBean
    GameServiceLayer gameServiceLayer;

    @Autowired
    GameServiceController gameServiceController;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getGameSequence_GivesASequenceBack_IfConnectionToOtherServiceExists() {
        //setup data
        String courseId = "1";
        List<Integer> sequenceList = new ArrayList<>();
        sequenceList.add(1);
        sequenceList.add(2);
        sequenceList.add(3);
        sequenceList.add(4);

        //mock the request
        Mockito.when(gameServiceLayer.getTheLengthForAGivenCourse(courseId)).thenReturn(sequenceList);
        //Do get the sequence
        ResponseEntity<GameSequence> getGameSequenceResponse = gameServiceController.getGameSequence(courseId);
        //Check if logic has been kept

        Assertions.assertEquals(courseId, getGameSequenceResponse.getBody().getCourseid(), "CourseId should be the same");
        Assertions.assertEquals(sequenceList, getGameSequenceResponse.getBody().getSequence(), "Length of sequence should be the same");
        Assertions.assertEquals(200, getGameSequenceResponse.getStatusCodeValue(), "Return code should be 200.");
    }
}