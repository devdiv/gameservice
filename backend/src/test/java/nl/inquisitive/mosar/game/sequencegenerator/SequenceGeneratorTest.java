package nl.inquisitive.mosar.game.sequencegenerator;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;

@ExtendWith(SpringExtension.class)
class SequenceGeneratorTest {


    @Test
    void getAnSequence_GivenAMaxLength_ReturnsASequenceOfThatLength() {
        int maxLength = 100;
        SequenceGenerator sequenceGenerator = new SequenceGenerator(maxLength);
        Assertions.assertEquals(maxLength, sequenceGenerator.getGameSequence().size(), "Sequence length should be the same.");
        Assertions.assertEquals(maxLength - 1, Collections.max(sequenceGenerator.gameSequence), "Length should contain the max - 1");
        Assertions.assertEquals(0, Collections.min(sequenceGenerator.gameSequence), "Array should start with 0");
    }
}