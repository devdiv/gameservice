package nl.inquisitive.mosar.game.service;

import nl.inquisitive.mosar.game.maxsequence.CurrentMaxSequence;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

@SpringBootTest(classes = GameServiceLayer.class)
@ExtendWith(SpringExtension.class)
class GameServiceLayerTest {

    @MockBean
    FlashcardRequestService flashcardRequestService;

    @Autowired
    GameServiceLayer gameServiceLayer;

    //mock the request
    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void getTheLengthForAGivenCourse_GivesASequence_IfRequestIsNew() {
        String courseId = "1";
        int lengthOfTheGame = 100;
        //Mock the request to the other service
        Mockito.when(flashcardRequestService.requestTheMaxForACourseFromFlashcardService(courseId)).thenReturn(String.valueOf(lengthOfTheGame));
        //Put in length, get out the length
        List<Integer> resultFromGameServiceLayer = gameServiceLayer.getTheLengthForAGivenCourse(courseId);
        //length should still be the same
        Assertions.assertEquals(lengthOfTheGame, resultFromGameServiceLayer.size(), "Length of sequence should be the same.");
    }

    @Test
    void getTheLengthForAGivenCourse_GivesASequence_IfCurrentInstanceIsOlderThen15Minutes() {
        //Put in length, get out the length
        String courseId = "1";
        int lengthOfTheGame = 100;
        String cleanedBody = "100";
        Instant time15MinutesAgo = Instant.now().minus(17, ChronoUnit.MINUTES);
        //Mock the request to the other service
        Mockito.when(flashcardRequestService.requestTheMaxForACourseFromFlashcardService(courseId)).thenReturn(String.valueOf(lengthOfTheGame));
        //Initialise the singleton with a time that is older then 15 minutes
        CurrentMaxSequence.getInstance().addCollectionToSingleton(courseId, cleanedBody, time15MinutesAgo);
        //Put in length, get out the length
        List<Integer> resultFromGameServiceLayer = gameServiceLayer.getTheLengthForAGivenCourse(courseId);
        //length should still be the same
        Assertions.assertEquals(lengthOfTheGame, resultFromGameServiceLayer.size(), "Length of sequence should be the same.");
    }

    @Test
    void getTheLengthForAGivenCourse_GivesASequence_IfRequestIsSecondRequestWithin15Minutes() {
        //Put in length, get out the length
        String courseId = "1";
        int lengthOfTheGame = 100;
        String cleanedBody = "100";
        Instant timeWithin15Minutes = Instant.now();
        //Mock the request to the other service
        Mockito.when(flashcardRequestService.requestTheMaxForACourseFromFlashcardService(courseId)).thenReturn(String.valueOf(lengthOfTheGame));
        //Initialise the singleton with a time that is within 15 minutes
        CurrentMaxSequence.getInstance().addCollectionToSingleton(courseId, cleanedBody, timeWithin15Minutes);
        //Put in length, get out the length
        List<Integer> resultFromGameServiceLayer = gameServiceLayer.getTheLengthForAGivenCourse(courseId);
        //length should still be the same
        Assertions.assertEquals(lengthOfTheGame, resultFromGameServiceLayer.size(), "Length of sequence should be the same.");
    }

    @Test
    void getTheLengthForAGivenCourse_GivesASequence_IfRequestDoesNotExistsInSingletonButIsInitiated() {
        //Put in length, get out the length
        //Existing set
        String courseId = "1";
        int lengthOfTheGame = 100;
        String cleanedBody = "100";
        Instant timeWithin15Minutes = Instant.now();
        //New Set
        String courseIdSecondSetToBeRequested = "2";
        int lengthOfTheGameSecondSetToBeRequested = 200;

        //Mock the request to the other service
        Mockito.when(flashcardRequestService.requestTheMaxForACourseFromFlashcardService(courseIdSecondSetToBeRequested)).thenReturn(String.valueOf(lengthOfTheGameSecondSetToBeRequested));
        //Initialise the singleton
        CurrentMaxSequence.getInstance().addCollectionToSingleton(courseId, cleanedBody, timeWithin15Minutes);
        //Put in length, get out the length
        List<Integer> resultFromGameServiceLayer = gameServiceLayer.getTheLengthForAGivenCourse(courseIdSecondSetToBeRequested);
        //length should still be the same
        Assertions.assertEquals(lengthOfTheGameSecondSetToBeRequested, resultFromGameServiceLayer.size(), "Length of sequence should be the same.");
    }
}