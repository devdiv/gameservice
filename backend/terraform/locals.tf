locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]
  }
}

locals {
  cidr_blocks = {
    private = [for s in data.aws_subnet.private : s.cidr_block]
    public  = [for s in data.aws_subnet.public : s.cidr_block]
  }
}

locals {
  roles = {
    game_task = {
      role = {
        name               = "game-ecs-task-role-${var.environment}"
        description        = "game role tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "game-ecs-task-policy-${var.environment}"
        description = "Policy for Game task role"
        policy      = file("./iam_spec_files/game_ecs_task_policy.json")
      }
      policy_attachment = {
        name = "game-ecs-task-policy-attachment"
      }
    }
    game_task_execution = {
      role = {
        name               = "game-ecs-task-execution-role-${var.environment}"
        description        = "role to execute the game ecs tasks"
        assume_role_policy = file("./iam_spec_files/assume_role_policy_ecs.json")
      }
      policy = {
        name        = "game-ecs-task-execution-policy-${var.environment}"
        description = "Policy to execute ecs game tasks"
        policy      = file("${path.root}/iam_spec_files/game_ecs_task_execution_policy.json")
      }
      policy_attachment = {
        name = "game-ecs-task-execution-policy-attachment"
      }
    }
  }
}

locals {
  security_groups = {
    game = {
      name        = "game_sg"
      vpc_id      = data.aws_vpc.mosar.id
      description = "security group access to the game service"
      ingress_sg  = {}
      ingress_cidr = {
        http = {
          from        = 8080
          to          = 8080
          protocol    = "tcp"
          cidr_blocks = local.cidr_blocks.private
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  game_names = {
    name           = "game"
    container_name = "game-${var.environment}"
    namespace_id   = regex("/(?P<id>.*)", data.aws_route53_zone.zone.linked_service_description).id
    namespace_name = data.aws_route53_zone.zone.name
  }
}


locals {
  apps = {
    game = {
      name                    = "mosar-game"
      desired_count           = 1
      container_definitions   = templatefile("./mosar-game-container.tpl", local.container_vars)
      subnet_ids              = local.subnets.private
      security_groups         = module.security_groups.security_groups["game"].*.id
      assign_public_ip        = false
      family                  = "mosar-game-family"
      use_load_balancer       = false
      lb_target_group_arn     = ""
      lb_container_name       = ""
      lb_container_port       = ""
      ecs_task_execution_role = module.iam.role["game_task_execution"]
      ecs_task_role           = module.iam.role["game_task"]
      cpu                     = 256
      memory                  = 512
      cluster                 = data.aws_ecs_cluster.cluster
      discovery_service = {
        name              = local.game_names.name
        namespace_id      = local.game_names.namespace_id
        ttl               = 30
        failure_threshold = 5
      }
      autoscaling = {
        max_capacity = 3
        min_capacity = 1
      }
      autoscaling_policies = {
        memory = {
          name                   = "game_memory_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageMemoryUtilization"
          target_value           = 80
        }
        cpu = {
          name                   = "game_cpu_autoscaling_policy"
          predefined_metric_type = "ECSServiceAverageCPUUtilization"
          target_value           = 60
        }
      }
    }
  }
}

locals {
  container_vars = {
    SPRING_PROFILES_ACTIVE = var.spring_profiles_active
    GAME_IMAGE             = var.game_image
    GAME_IMAGE_TAG         = var.game_image_tag
    ENVIRONMENT            = var.environment
    MOSAR_LOG_GROUP_NAME   = "mosar-logs-${var.environment}"
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}

locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}

