# game app/variables.tf

variable "environment" {}
variable "spring_profiles_active" {}
variable "game_image" {}
variable "game_image_tag" {}
variable "log_group_name" {}
variable "managed_by" {
  default = "jenkins_pipeline_game"
}
