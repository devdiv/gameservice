final: prev:
{
  dotnet-sdk_3 = prev.dotnet-sdk_3.overrideAttrs(oldAttrs: rec {
    version = "3.1.407";
    src = prev.fetchurl {
      url = "https://dotnetcli.azureedge.net/dotnet/Sdk/3.1.407/dotnet-sdk-3.1.407-linux-x64.tar.gz";
      sha512 =   "065q1xsz47zwpbmqfpmxwy029imhzh4hsnvbnfnjihq4r153fz1zh1h6rv9nq7l6kj6hhdsrk92gk992954m2dm7q5dyf2a8rhi1imr";
    };
  });
}
